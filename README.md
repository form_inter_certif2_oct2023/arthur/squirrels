
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels

<!-- badges: start -->
<!-- badges: end -->

Ce package a été créé dans le cadre d’une formation N2

Ce package est une boite a outils pour analyser les ecureuils et en plus
il est versionné !!

## Installation

You can install the development version of squirrels like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrels)
## basic example code
```

``` r
get_message_fur_color("Black")
#> We will focus on Black squirrels
```

## Code of Conduct

Please note that the squirrels project is released with a [Contributor
Code of
Conduct](https://contributor-covenant.org/version/2/1/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.
